#include <iostream>
#include <limits>

using namespace std;

#include "calculate.h"


int Calculate::Calculation(CalcType op, int a, int b) {
    if (op == ADD) {
        return a + b;
    } else if (op == MINUS) {
        return a - b;
    } else if (op == MULTIPLE) {
        return a * b;
    } else {
        if (b == 0) {
            cout << "CANNO Divided by 0" << endl;
            return std::numeric_limits<int>::max();
        }
        return a / b;
    }
}

double Calculate::Calculation(CalcType op, double a, double b) {
    if (op == ADD) {
        return a + b;
    } else if (op == MINUS) {
        return a - b;
    } else if (op == MULTIPLE) {
        return a * b;
    } else {
        if (b == 0) {
            cout << "CANNO Divided by 0" << endl;
            return std::numeric_limits<double>::max();
        }
        return a / b;
    }
}

int main() {
    int first_number, second_number;

    cout << "Enter two number: ";
    cin >> first_number >> second_number;

    Calculate cal;

    cout << "ADD      result: " << cal.Calculation(ADD, first_number, second_number) << endl;
    cout << "MINUS    result: " << cal.Calculation(MINUS, first_number, second_number) << endl;
    cout << "MULTIPLE result: " << cal.Calculation(MULTIPLE, first_number, second_number) << endl;
    cout << "DIVIDE   result: " << cal.Calculation(DIVIDE, first_number, second_number) << endl;

    return 0;
}
