cmake_minimum_required (VERSION 3.5)

project(first_program)
add_executable( ${PROJECT_NAME} 01_first_program.cpp )
install( TARGETS ${PROJECT_NAME} DESTINATION bin )

project(calculate)
add_executable( ${PROJECT_NAME} 02_calculate.cpp )
install( TARGETS ${PROJECT_NAME} DESTINATION bin )
